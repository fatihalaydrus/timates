from django.shortcuts import render, get_object_or_404, redirect
from .models import Venue
from .forms import VenueBookForm

# Create your views here.
def index(request):
    form = VenueBookForm(request.POST or None)
    if request.method == "POST" and form.is_valid:
            pass #todo -> integrasi booking dengan findpartner / turnamen
    response = {
        'venues': Venue.objects.all() if Venue.objects.all() else None,
        'venue_book_form' : form
    }
    return render(request, 'venue_list.html', response)

def venue_page(request, id):
    response = {
        'venue' : get_object_or_404(Venue, id=id),
    }
    return render(request, 'venue_page.html', response)