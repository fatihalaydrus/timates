'''
URLs for Venue
'''
from django.urls import path
from .views import index, venue_page

urlpatterns = [
    path('', index, name="venue_list"),
    path('<int:id>', venue_page, name="venue_page"),
]
