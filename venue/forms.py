from django import forms
from .models import Venue

class VenueBookForm(forms.Form):
    datetime = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'])
    venue = forms.ModelChoiceField(queryset=Venue.objects.all().order_by('name'))

    def __init__(self, *args, **kwargs):
        super(VenueBookForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control bg-primary'
            visible.field.widget.attrs['autocomplete'] = 'off'