from django.shortcuts import render, redirect

# Create your views here.
from .forms import PostForm
from .models import PostModel

def find(request):
    post_form = PostForm(request.POST or None)
    if request.method == "POST":
        if post_form.is_valid():
            post_form.save()
            return redirect('/findpartner/')
    else:
        post_form = PostForm()
            
    posts = PostModel.objects.all()
    contexts = {
        'posts': posts,
        'form': post_form,
    }
    return render(request, 'find.html', contexts)

def success(request):
    return render(request, 'success.html')
