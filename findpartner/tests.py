from django.test import TestCase, Client
from django.urls import resolve
from .views import find, success
from .models import PostModel
from .forms import PostForm
from django.apps import apps
from findpartner.apps import FindpartnerConfig
import unittest
# Create your tests here.

class Testing(TestCase):
    def setup(self):
        games = PostModel(
            game_title = 'Title',
            game_type = 'Futsal',
            slot = '16',
            location = 'Futasl Field',
            date = 'Oct. 20, 2019',
            time = '07:30',
            contact = '0812xxxxxxx (Bambank)'
        )
        return games

        #Test Views
    def test_game_creation(self):
        game = self.setup()
        self.assertTrue(isinstance(game, PostModel))
        self.assertEqual(str(game), "{}.{}".format(game.id, game.game_title))

    def test_game_url_is_exist(self):
        response = Client().get('/findpartner/')
        self.assertEqual(response.status_code,200)

    def test_game_using_findpartner_templates(self):
        response = Client().get('/findpartner/')
        self.assertTemplateUsed(response, 'find.html')
        
    def test_game_using_find_func(self):
        found = resolve('/findpartner/')
        self.assertEqual(found.func, find)

        #Test Landing Page
    def test_landing_page_url_is_exist(self):
        game = self.setup()
        response = Client().get('/findpartner/success/')
        self.assertEqual(response.status_code,200)

    def test_landing_page_using_success_template(self):
        game = self.setup()
        response = Client().get('/findpartner/success/')
        self.assertTemplateUsed(response, 'success.html')

    def test_landing_page_using_landing_func(self):
        game = self.setup()
        found = resolve('/findpartner/success/')
        self.assertEqual(found.func, success)

         #app.py test
    def test_apps(self):
        self.assertEqual(FindpartnerConfig.name, 'findpartner')
        self.assertEqual(apps.get_app_config('findpartner').name, 'findpartner')

        #form test

    # def test_form_valid(self):
    #     form_data = {
    #         'game_title' : 'Title',
    #         'game_type' : 'Futsal',
    #         'slot' : 16,
    #         'location' : 'Futsal Field',
    #         'date' : '2019-10-20',
    #         'time' : '07:30:00',
    #         'contact' : '0812xxxxxxx (Bambank)'
    #     }
    #     form = PostForm(data=form_data)
    #     self.assertTrue(form.is_valid())

    # def test_form_invalid(self):
    #     form_data = {
    #         'game_title' : 'Title',
    #         'game_type' : 'Futsal',
    #         'slot' : '16',
    #         'location' : 'Futasl Field',
    #         'date' : 'Oct. 20, 2019',
    #         'time' : '07:30',
    #         'contact' : '0812xxxxxxx (Bambank)'
    #     }
    #     form = PostForm(data=form_data)
    #     self.assertFalse(form.is_valid())

    def test_form_can_be_used(self):
        web = Client().post('/findpartner/', 
        data={'game_title' : 'Title',
            'game_type' : 'Futsal',
            'slot' : 16,
            'location' : 'Futasl Field',
            'date' : '2019-10-20',
            'time' : '07:30:00',
            'contact' : '0812xxxxxxx (Bambank)'})
        self.assertTrue(PostModel.objects.filter(game_title="Title").exists())



    
