from django.urls import path
from . import views

urlpatterns = [
    path('', views.find, name='findpartner'),
    path('success/', views.success, name='success')
]
