from django.test import TestCase, Client
from django.urls import resolve
from .views import champ, land
from .models import makeTournament
from .forms import tournamentForm
import tempfile
from django.apps import apps
from championship.apps import ChampionshipConfig
import unittest

# Create your tests here.

class myTest(TestCase):
    # Models test
    def test_model_new_tournament(self):
        new_tournament = makeTournament.objects.create(
            event = 'tournamentTest',
            sport = 'badmintonTest',
            player = '2',
            location = 'GBKTest',
            contact = '0812test',
            date = '2019-09-13',
            time = '09:00'
        )
        return new_tournament

    # Views test
    def test_create_tournament(self):
        tournament = self.test_model_new_tournament()
        self.assertTrue(isinstance(tournament, makeTournament))

    def test_championship_url_is_exist(self):
        response = Client().get('/tournament/')
        self.assertEqual(response.status_code,200)

    '''
    def test_tournament_using_turnament_templates(self):
        response = Client().get('/tournament/')
        self.assertTemplateUsed(response, 'championship.html')
    '''

    def test_tournament_using_index_func(self):
        found = resolve('/tournament/')
        self.assertEqual(found.func, champ)

    
    # test tournament page

    def test_tournament_page_url_is_exist(self):
        tournament = self.test_model_new_tournament()
        response = Client().get('/tournament/')
        self.assertEqual(response.status_code,200)

    def test_tournament_page_using_tournament_page_templates(self):
        tournament = self.test_model_new_tournament()
        response = Client().get('/tournament/')
        self.assertTemplateUsed(response, 'championship.html')

    def test_landing_page_url_is_exist(self):
        tournament = self.test_model_new_tournament()
        response = Client().get('/tournament/landing/')
        self.assertEqual(response.status_code,200)

    def test_landing_page_using_landing_page_templates(self):
        tournament = self.test_model_new_tournament()
        response = Client().get('/tournament/landing/')
        self.assertTemplateUsed(response, 'champ-landing.html')

    def test_tournament_page_using_tournament_page_func(self):
        tournament = self.test_model_new_tournament()
        found = resolve('/tournament/')
        self.assertEqual(found.func, champ)
   
    def test_landing_page_using_landing_page_func(self):
        tournament = self.test_model_new_tournament()
        found = resolve('/tournament/landing/')
        self.assertEqual(found.func, land)
   
    # test app.py
    def test_apps(self):
        self.assertEqual(ChampionshipConfig.name, 'championship')
        self.assertEqual(apps.get_app_config('championship').name, 'championship')

    def test_form_is_exist(self):
        self.response = Client().get('/tournament/')
        self.assertIn('Tournament', self.response.content.decode())

    def test_form_can_be_used(self):
        web = Client().post('/tournament/', 
        data = {
            'event':'Asian Games',
            'sport' : 'BD',
            'player' : 2,
            'location' : 'GBKTest',
            'contact' : '081254345',
            'date_year' : '2019',
            'date_month' :'09',
            'date_day':'12',
            'time' : '09:00',})
        self.assertTrue(makeTournament.objects.filter(event="Asian Games").exists())



