from django.shortcuts import render, redirect
from . import forms, models

# Create your views here.
def champ(request):
    if request.method == "POST":
        form = forms.tournamentForm(request.POST)

        # if 'id' in request.POST:
        #     models.makeTournament.objects.get(id=request.POST['id']).delete()
        #     return redirect('/tournament/')

        if form.is_valid():
            tournamentData = models.makeTournament(
                event = form.data['event'],
                player = form.data['player'],
                location = form.data['location'],
                contact = form.data['contact'],
                date = form.data['date_year'] + "-" + form.data['date_month'] + "-" + form.data['date_day'],
                time = form.data['time'],
            )
            tournamentData.save()
            return redirect('/tournament/')
    else:
        form = forms.tournamentForm()

    tournament_context = {
        'tournament' : models.makeTournament.objects.all().values(),
        'form' : form,
    }

    return render(request, 'championship.html', tournament_context)

def land(request):
    return render(request, 'champ-landing.html')