from django import forms
from championship import models

years = [x for x in range(2019, 2025)]

class tournamentForm(forms.Form):
    event  = forms.CharField(
        label = "Event",
        max_length = 100,
        required = True,
        widget = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder' : 'National Basketball Development League'})
    )

    sport = forms.ChoiceField(
        label = 'Sport',
        choices = [
            ('BB', 'Basketball'),
            ('BD', 'Badminton'),
            ('BS', 'Baseball'),
            ('FB', 'Football'),
            ('FS', 'Futsal'),
            ('CY', 'Cycling'),
            ('PP', 'Ping Pong'),
            ('RN', 'Running'),
            ('SW', 'Swimming'),
            ('TS', 'Tennis'),
            ('VB', 'Volleyball'),
        ],
        widget = forms.Select(attrs = {'class' : 'form-control form-control-sm', 'placeholder' : 'Cycling'}),
    )

    player = forms.IntegerField(
        label = 'Player',
        min_value = 2,
        required = True,
        widget = forms.TextInput(attrs={'class' : 'form-control form-control-sm', 'placeholder' : '2'}),
    )

    location = forms.CharField(
        label = 'Location',
        max_length = 50,
        required = True,
        widget = forms.TextInput(attrs ={'class' : 'form-control form-control-sm', 'placeholder': 'Gelora Bung Karno'}),
    )

    contact = forms.CharField(
        label = 'Contact',
        max_length = 50,
        required = True,
        widget = forms.TextInput(attrs ={'class' : 'form-control form-control-sm', 'placeholder': '0812xxxxxxxx'})
    )

    date = forms.DateField(
        label = "Date",
        widget = forms.SelectDateWidget(years = years, attrs = {'class' : 'form-control-sm'})
    )


    time = forms.TimeField(
        label = 'Time',
        input_formats = ['%H:%M'],
        widget = forms.TextInput(attrs={'class' : 'form-control form-control-sm', 'placeholder' : '00:00'}),
    )