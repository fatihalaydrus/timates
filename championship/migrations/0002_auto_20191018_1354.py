# Generated by Django 2.2.5 on 2019-10-18 13:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('championship', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='maketournament',
            name='date',
            field=models.DateField(),
        ),
    ]
