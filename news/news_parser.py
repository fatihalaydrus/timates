import requests
import environ
import json

class newsAPI():
    def __init__(self):
        environ.Env.read_env()
        self.env = environ.Env(NEWSAPI_KEY=(str, 'bogus_key'))
        self.url = 'https://newsapi.org/v2/top-headlines?'
        self.source = 'sources=espn&' #fox-sport espn
        self.key = 'apiKey=' + self.env('NEWSAPI_KEY')

    def change_sources(self, sources):
        self.sources = 'sources=' + sources + '&'

    def change_key(self, key):
        self.key = 'apiKey='+key

    def get(self):
        response = None
        response_espn = requests.get(self.url+'sources=espn&'+self.key)
        if (response_espn.status_code==200 and response_espn.json()['status']=='ok'):
            resp = response_espn.json()
            response = resp['articles']
        else:
            print(response_espn.status_code, response_espn.json()['status'], response_espn.json()['message'])
            placeholder = {'url':"#", 
            'urlToImage':"https://webhostingmedia.net/wp-content/uploads/2016/12/500-internal-server-error.png", 
            'title':'500 internal server error', 
            'description':'there is something wrong while trying to retreive news, please try again later.',
            }
            response = [placeholder, placeholder, placeholder]
        # response_fox_sport = requests.get(self.url+'sources=fox-sport&'+self.key)
        # if ( response_fox_sport.status_code==200 and  response_fox_sport.json()['status']=='ok'):
        #     resp = response_fox_sport.json()
        #     response['fox_sport'] = resp['articles'][0]
        # else:
        #     pass
        # response_bbc_sport = requests.get(self.url+'sources=bbc-sport&'+self.key)
        # if (response_bbc_sport.status_code==200 and response_bbc_sport.json()['status']=='ok'):
        #     resp = response_bbc_sport.json()
        #     response['bbc_sport'] = resp['articles'][0]
        # else:
        #     pass
        return (response)