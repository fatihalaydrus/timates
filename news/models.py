from django.db import models
from django.utils import timezone

class Blog(models.Model):
    author = models.CharField(max_length=30)
    posting_time = models.DateField(default=timezone.now)  #timezone.now()
    title = models.CharField(max_length=50)
    content = models.TextField()