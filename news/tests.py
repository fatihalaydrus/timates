from django.test import TestCase, Client
from django.utils import timezone
from .views import index, addBlog
from .models import Blog
from .apps import NewsConfig
from django.apps import apps
import news.news_parser as parser

class NewsTest(TestCase):
    #test models
    def create_blog(self, 
                    author="John doe", title = "Has seraphina regain her power??",
                    content = '''Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis ut lectus turpis. Proin dapibus vestibulum justo, pharetra consectetur odio aliquam non. Nam vestibulum, erat id auctor dignissim, leo nunc laoreet nisi, vitae fermentum risus dolor ac libero. Aenean euismod elit nisi, ac fringilla magna fermentum quis. Donec dui metus, ultrices sit amet hendrerit quis, pharetra a lectus. Aliquam erat volutpat. Etiam at pellentesque massa, at fermentum neque. Vestibulum sagittis metus non libero finibus, finibus venenatis ex scelerisque. Nam pretium ut arcu lobortis faucibus. Duis quis libero suscipit, lobortis massa sed, interdum leo. Aenean faucibus, neque vel lobortis lobortis, nulla risus tempor est, a sodales augue purus non nibh. Etiam convallis vitae libero at egestas. Curabitur et lorem eros. Donec in ligula orci. Suspendisse porta mi non mauris rutrum feugiat. Curabitur efficitur leo sit amet nibh euismod, sit amet cursus turpis rhoncus.

Pellentesque ut dignissim nunc, at finibus dui. In laoreet leo nisi, eu egestas quam consequat in. Fusce id aliquet justo. Vestibulum pretium elit vitae est sodales interdum ut sed sapien. Duis semper bibendum nulla vel vestibulum. Phasellus sollicitudin nunc at dui pulvinar suscipit. Cras pulvinar mauris sit amet rhoncus venenatis. Cras euismod felis et quam semper, at ornare metus rutrum. Praesent sed lacus at magna tempus tristique ut a nisi. Vivamus venenatis sollicitudin massa, nec sodales lorem tristique at. Integer mattis metus et accumsan semper.

Sed ac varius neque, in blandit turpis. Suspendisse viverra scelerisque nulla, ac viverra elit tempus vel. Etiam semper urna ut dapibus semper. Aliquam lacus nibh, bibendum sed feugiat eget, fermentum aliquam tortor. Nullam a ligula sed tortor mollis blandit quis eget turpis. Donec auctor ante nec dolor commodo sollicitudin. Integer eu volutpat metus. Duis vehicula auctor neque, vitae aliquam metus ullamcorper quis. Phasellus lacinia, lacus eu tristique maximus, lorem justo sodales nisi, eu ultrices massa sem ut mauris. Donec dictum venenatis lorem vitae dictum. Mauris a nibh ut quam suscipit ultricies. Phasellus id interdum neque. Etiam nunc ipsum, viverra sed nibh ac, scelerisque volutpat mauris. Donec eget fringilla dolor. Cras ut finibus metus.

Pellentesque sodales luctus turpis, ac bibendum diam dapibus vestibulum. Suspendisse potenti. Cras efficitur tempor fringilla. Integer faucibus sem eget dui semper, a varius eros bibendum. Donec ac semper mi. Morbi imperdiet sed sapien in varius. Donec posuere eget tellus sed aliquam. Fusce ac orci eu orci tristique auctor. Sed at augue enim. Curabitur sollicitudin tortor non nisi tristique, vitae tincidunt libero facilisis. Praesent at finibus leo, a sollicitudin diam. Aliquam quis dui velit. Quisque venenatis dolor ut lacus tempus elementum id vel libero.

Sed in viverra sapien, eget sagittis eros. Etiam fringilla urna tellus, ornare dictum est eleifend sit amet. Pellentesque pulvinar, diam et ornare interdum, metus mauris posuere eros, eu tristique neque mauris vel nulla. Aenean a maximus mi, in fermentum libero. Duis et ligula in orci viverra sodales sed ut dolor. Integer purus nulla, gravida sed sollicitudin et, maximus sed sem. Sed sed enim eget arcu sodales facilisis. Morbi ac justo id tellus tincidunt aliquet quis id nulla. Nullam vitae scelerisque augue. Cras vitae risus nulla. Aliquam sit amet massa mi. Mauris bibendum finibus orci.'''):
        return Blog.objects.create(author=author, title=title, content = content)

    def test_blog_creation(self):
        blog = self.create_blog()
        self.assertTrue(isinstance(blog, Blog))

    #test urls
    def test_news_is_using_templates(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'news_index.html')

    def test_add_blog_url_is_using_templates(self):
        response = Client().get('/news/add-blog/')
        self.assertTemplateUsed(response, 'add_blog.html')

    #test views
    def test_news_url_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code,200)        


    def test_add_blog_url_is_exist(self):
        response = Client().get('/news/add-blog/')
        self.assertEqual(response.status_code,200)     

    def test_Blog_creation_correct_request_header(self):
        response = Client().post('/news/add-blog/')
        self.assertEqual(response.status_code, 200)
        self.assertIn("failed", response.content.decode())
        self.assertNotIn("success", response.content.decode())

    def test_Blog_creation_valid(self):
        response = Client().post('/news/add-blog/', data={"author" : "John Doe", "title" : "Has seraphina regain her power??", "content" : "cya in season 2 :v"})
        self.assertEqual(response.status_code, 200)
        self.assertIn("success", response.content.decode())
        self.assertNotIn("failed", response.content.decode())
    def test_NewsConfig(self):
        self.assertEqual(NewsConfig.name, 'news')
        self.assertEqual(apps.get_app_config('news').name, 'news')

    #test news parser
    def test_change_source(self):
        apiCaller = parser.newsAPI()
        apiCaller.change_sources('fox-sports')
        placeholder = {'url':"#", 
            'urlToImage':"https://webhostingmedia.net/wp-content/uploads/2016/12/500-internal-server-error.png", 
            'title':'500 internal server error', 
            'description':'there is something wrong while trying to retreive news, please try again later.',
            }
        self.assertNotEquals(apiCaller.get(), [placeholder, placeholder, placeholder])
        
    def test_change_key(self):
        apiCaller = parser.newsAPI()
        apiCaller.change_key('bogus_key')
        self.assertEqual(apiCaller.key, 'apiKey='+'bogus_key')

    def test_client_error(self):
        apiCaller = parser.newsAPI()
        placeholder = {'url':"#", 
            'urlToImage':"https://webhostingmedia.net/wp-content/uploads/2016/12/500-internal-server-error.png", 
            'title':'500 internal server error', 
            'description':'there is something wrong while trying to retreive news, please try again later.',
            }
        apiCaller.change_key('bogus_key')
        self.assertEqual(apiCaller.key, 'apiKey='+'bogus_key')
        self.assertEqual(apiCaller.get(), [placeholder, placeholder, placeholder])
