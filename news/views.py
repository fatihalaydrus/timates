from django.shortcuts import render
from django.http import HttpResponse
from .models import Blog
from .forms import AddBlogForm
import news.news_parser as parser
import json


def index(request):
    news_api = parser.newsAPI()
    response = {}
    response['news'] = news_api.get()
    response['add_blog_form'] = AddBlogForm
    blog = Blog.objects.all().values()
    response['blog'] = blog
    response['blog_length'] = blog.count()
    print(blog.count())
    return render(request, 'news_index.html', response)

def addBlog(request):
    response = {'save_status':'', 'message':''}
    if(request.method == 'POST'):
        form = AddBlogForm(request.POST)
        if (form.is_valid()):
            author = form.cleaned_data['author']
            title = form.cleaned_data['title']
            content = form.cleaned_data['content']
            
            blog = Blog(
                author = author,
                title = title,
                content = content,
            )

            blog.save()

            response['save_status'] = 'success'
            response['messages'] = 'none'
        else:
            response['save_status'] = 'failed'
            response['messages'] = 'invalid data'
    else:
        response['save_status'] = 'failed'
        response['messages'] = 'invalid HTTP method'

    return render(request, 'add_blog.html', response)