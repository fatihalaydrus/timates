from django import forms
from .models import Blog

class AddBlogForm(forms.Form):
    author_attrs = {
        'type':'text',
        'class': 'form-control bg-secondary',
        'placeholder': 'Your name'
    }

    title_attrs = {
        'type':'text',
        'class': 'form-control bg-secondary',
        'placeholder': 'title'
    }

    content_attrs = {
        'type':'text',
        'class': 'form-control bg-secondary',
        'placeholder': 'start writing awesone stories...'
    }

    author = forms.CharField(label='Author', max_length=30,  required=True, widget=forms.TextInput(attrs=author_attrs))
    title = forms.CharField(label='Title', max_length=50,  required=True, widget=forms.TextInput(attrs=title_attrs))
    content = forms.CharField(label='Content', required=True, widget=forms.Textarea(attrs=content_attrs))