from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='news'),
    path('add-blog/', views.addBlog, name='add-blog')
]